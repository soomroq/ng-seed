![alt tag](https://lh3.googleusercontent.com/XNlQ78IEAdgsZxHRKTFX_ZRj5QhCEdqdaMsBUK8COA=w100-h172-no)

## Synopsis
Who? This seed project is independent from the server, allowing you to use any server side framework!

What? This is a seed project for AngularJS allowing you to get a head start on your official application!

When? This was created in 2015 but is expected to be maintained and upgraded to AngularJS 2.0!

Where? This was created in sunny San Diego at [Seamgen](http://www.seamgen.com) headquarters!

Why? Because there needed to be a better, best practices involved, and efficient client side AngularJS seed

## Prerequisites
Make sure you have installed all of the following prerequisites on your development machine:
* Node.js - [Download & Install Node.js](http://www.nodejs.org/download/) and the npm package manager. If you encounter any problems, you can also use this [GitHub Gist](https://gist.github.com/isaacs/579814) to install Node.js.
* Bower - You're going to use the [Bower Package Manager](http://bower.io/) to manage your front-end packages. Make sure you've installed Node.js and npm first, then install bower globally using npm:

```bash
$ npm install -g bower
```

* Grunt - You're going to use the [Grunt Task Runner](http://gruntjs.com/) to automate your development process. Make sure you've installed Node.js and npm first, then install grunt globally using npm:

```bash
$ npm install -g grunt-cli
```

## Downloading ng-seed
There are several ways you can get the ng-seed boilerplate:

### Cloning The BitBucket Repository
You can also use Git to directly clone the ng-seed repository:
```bash
$ git clone https://soomroq@bitbucket.org/soomroq/ng-seed.git ng-seed
```
This will clone the latest version of the ng-seed repository to a **ng-seed** folder.

## Quick Install
Once you've downloaded the boilerplate and installed all the prerequisites, you're just a few steps away from starting to develop your ng-seed application.

The first thing you should do is install the Node.js dependencies. The boilerplate comes pre-bundled with a package.json file that contains the list of modules you need to start your application. To learn more about the modules installed visit the NPM & Package.json section.

To install Node.js dependencies you're going to use npm again. In the application folder run this in the command-line:

```bash
$ npm install
```

To install Bower dependencies you're going to use bower. In the application folder run this in the command-line:

```bash
$ bower install
```

This command does a few things:
* First it will install the dependencies needed for the application to run.
* If you're running in a development environment, it will then also install development dependencies needed for testing and running your application.
* Finally, when the install process is over, npm will initiate a bower install command to install all the front-end modules needed for the application.

## Running Your Application
After the install process is over, you'll be able to run your application using Grunt. Just run grunt default task:

```bash
$ grunt
```

Your application should run on port 8080, so in your browser just go to [http://localhost:8080](http://localhost:8080)

That's it! Your application should be running. To proceed with your development, check the other sections in this documentation. Remember that the "articles" and "users" modules are for example and learning purposes only!
If you encounter any problems, try the Troubleshooting section.

## Testing Your Application
You can run the full test suite included with ng-seed with the test task:

```
$ grunt test
```

This will run both the tests (located in the public/modules/*/tests/).


## Build A Production Version
You can build your application to be minified for production use with this simple task:
```
$ grunt build
```

What this will do is create a new directory called "dist" and within this directory you will
find your production files. Enjoy and get creative!

## Contributors

[Qasim Soomro](https://github.com/qasimsoomro) &
[Amos Haviv](https://github.com/amoshaviv)

## License

(The MIT License)

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.